package com.space.game.android;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.space.game.GameScreen;
import com.space.game.SpaceGame;

import android.os.Bundle;

public class MainActivity extends AndroidApplication {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration conf = new AndroidApplicationConfiguration();
		initialize(new SpaceGame(1, 1, 1200, 800, 5), conf);
		GameScreen.android = true;
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		finish();
	}

}
