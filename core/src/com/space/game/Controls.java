package com.space.game;

import com.badlogic.gdx.Input.Keys;

public class Controls {
	public static int Up(int num) {
		int keyValue = 0;
		switch(num) {
		case 1:
			keyValue = Keys.UP;
			break;
		case 2:
			keyValue = Keys.W;
			break;
		case 3:
			keyValue = Keys.I;
			break;
		case 4:
			keyValue = Keys.NUMPAD_8;
			break;
		default:
			break;
		}
		return keyValue;
	}
	
	public static int Down(int num) {
		int keyValue = 0;
		switch(num) {
		case 1:
			keyValue = Keys.DOWN;
			break;
		case 2:
			keyValue = Keys.S;
			break;
		case 3:
			keyValue = Keys.K;
			break;
		case 4:
			keyValue = Keys.NUMPAD_5;
			break;
		default:
			break;
		}
		return keyValue;
	}
	
	public static int Left(int num) {
		int keyValue = 0;
		switch(num) {
		case 1:
			keyValue = Keys.LEFT;
			break;
		case 2:
			keyValue = Keys.A;
			break;
		case 3:
			keyValue = Keys.J;
			break;
		case 4:
			keyValue = Keys.NUMPAD_4;
			break;
		default:
			break;
		}
		return keyValue;
	}
	
	public static int Right(int num) {
		int keyValue = 0;
		switch(num) {
		case 1:
			keyValue = Keys.RIGHT;
			break;
		case 2:
			keyValue = Keys.D;
			break;
		case 3:
			keyValue = Keys.L;
			break;
		case 4:
			keyValue = Keys.NUMPAD_6;
			break;
		default:
			break;
		}
		return keyValue;
	}
	
	public static int Fire(int num) {
		int keyValue = 0;
		switch(num) {
		case 1:
			keyValue = Keys.CONTROL_RIGHT;
			break;
		case 2:
			keyValue = Keys.Q;
			break;
		case 3:
			keyValue = Keys.U;
			break;
		case 4:
			keyValue = Keys.NUMPAD_9;
			break;
		default:
			break;
		}
		return keyValue;
	}
	
	public static int Bomb(int num) {
		int keyValue = 0;
		switch(num) {
		case 1:
			keyValue = Keys.SHIFT_RIGHT;
			break;
		case 2:
			keyValue = Keys.E;
			break;
		case 3:
			keyValue = Keys.O;
			break;
		case 4:
			keyValue = Keys.NUMPAD_7;
			break;
		default:
			break;
		}
		return keyValue;
	}
}
