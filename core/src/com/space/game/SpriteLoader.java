package com.space.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;

public class SpriteLoader {
	//Background
	public static Texture backGround;
	//Explosion
	public static Array<TextureRegion> expRegion;
	//Laser beamz
	public static Texture laser;
	//Health bar
	public static Texture healthBar;
	//Ship
	public static Texture ship;
	//Aliens
	public static Texture roundAlien;
	public static Texture alienProjectile;
	//Mine
	public static Texture mine;
	//Health pack
	public static Texture healthPack;
	//Mine pack
	public static Texture minePack;
	//Sun
	public static Texture sun;
	//Planets
	public static Texture planetDesert;
	public static Texture planetMethane;
	public static Texture planetInferno;
	public static Texture planetTundra;
	//Fonts
	public static BitmapFont HUDFont;

	public static void loadSprites() {
		int spaceBackground = MathUtils.random(3) + 1;
		backGround = new Texture("space" + spaceBackground + ".png");
		laser = new Texture("laser.png");
		healthBar = new Texture("healthbar.png");
		ship = new Texture("Ship.png");
		roundAlien = new Texture("alien1.png");
		alienProjectile = new Texture("alien_beam.png");
		mine = new Texture("mine.png");
		healthPack = new Texture("health_pack.png");
		minePack = new Texture("mine_pack.png");
		sun = new Texture ("sun.png");
		planetDesert = new Texture("desert.png");
		planetMethane = new Texture("methane.png");
		planetInferno = new Texture("inferno.png");
		planetTundra = new Texture("tundra.png");
		HUDFont = new BitmapFont(Gdx.files.internal("font.fnt"));
		Texture expTexture = new Texture("explosion2.png");
		//Split the sprite sheet into an array
		TextureRegion[][] temp = TextureRegion.split(expTexture, 128, 128);
		expRegion = new Array<TextureRegion>(64);
		for(int i = 0; i < 8; i++) {
			for(int j = 0; j < 8; j++) {
				expRegion.add(temp[i][j]);
			}
		}
	}
	
	public static void dispose() {
		backGround.dispose();
		laser.dispose();
		healthBar.dispose();
		ship.dispose();
		mine.dispose();
		healthPack.dispose();
		minePack.dispose();
		sun.dispose();
		planetDesert.dispose();
		planetMethane.dispose();
		planetInferno.dispose();
		planetTundra.dispose();
	}
}
