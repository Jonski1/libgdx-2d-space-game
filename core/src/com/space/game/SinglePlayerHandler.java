package com.space.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class SinglePlayerHandler {
	
	private GameScreen game;
	private int wave;
	private int score;
	public Label label;
	private float timeToWait;
	private float timeWaited;
	
	public SinglePlayerHandler(GameScreen game) {
		this.game = game;
		wave = 0;
		score = 0;
		timeToWait = 3f;
		timeWaited = 0f;
		Skin skin = new Skin();
		skin.add("default" , SpriteLoader.HUDFont);
		Label.LabelStyle style = new Label.LabelStyle();
		style.font = skin.getFont("default");
		style.fontColor = Color.WHITE;
		skin.add("default", style);
		label = new Label("Score: " + score, skin);
	}
	
	public void update(float delta) {
		game.score = score;
		if(game.noAliens()) {
			timeWaited += delta;
			if(timeWaited >= timeToWait) createAliens();
		}
	}
	
	private void createAliens() {
		timeWaited = 0;
		int len = (wave + 2) / 2;
		for(int i = 0; i < len; i++)
			game.addAlien(wave + 2);
		wave++;
	}
	
	public void updateScore(int score) {
		this.score += score;
		label.setText("Score: " + this.score);
	}
}
