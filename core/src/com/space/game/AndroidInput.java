package com.space.game;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;

public class AndroidInput {
	
	private Map<String, Button> buttons;
	
	public AndroidInput(float w, float h) {
		buttons = new HashMap<String, Button>();
		buttons.put("left", createButton("Left", 50, 50));
		buttons.put("right", createButton("Right", 200, 50));
		buttons.put("up", createButton("Up", 125, 100));
		buttons.put("down", createButton("Down", 125, 10));
		buttons.put("fire", createButton("Fire", w - 200, 50));
		buttons.put("bomb", createButton("Bomb", w - 400, 50));
	}
	
	private TextButton createButton(String name, float x, float y) {
		Skin s = new Skin();
		s.add("default", new BitmapFont());
		int pix_x = 32;
		int pix_y = 32;
		Pixmap pix = new Pixmap(pix_x, pix_y, Format.RGBA8888);
		pix.setColor(0, 0, 0, 0);
		pix.fill();
		pix.setColor(Color.WHITE);
		pix.drawRectangle(0, 0, pix_x, pix_y);
		pix.drawRectangle(1, 1, pix_x - 1, pix_y - 1);
		s.add("box", new Texture(pix));
		TextButtonStyle style = new TextButtonStyle();
		style.font = s.getFont("default");
		style.fontColor = Color.WHITE;
		style.up = s.newDrawable("box");
		style.down = s.newDrawable("box", Color.RED);
		s.add("default", style);
		TextButton b = new TextButton(name, s);
		b.setHeight(70);
		b.setWidth(70);
		b.setX(x);
		b.setY(y);
		return b;
	}
	
	@SuppressWarnings("unused")
	private ImageButton createImageButton(String texturePath, float x, float y) {
		Sprite s = new Sprite(new Texture(texturePath));
		ImageButton b = new ImageButton(new SpriteDrawable(s));
		b.setHeight(70);
		b.setWidth(70);
		b.setX(x);
		b.setY(y);
		return b;
	}
	
	public boolean isPressed(String button) {
		if(!buttons.containsKey(button)) return false;
		return buttons.get(button).isPressed();
	}
	
	public Button[] get() {
		Collection<Button> butts = buttons.values();
		Button[] arr = new Button[butts.size()];
		butts.toArray(arr);
		return arr;
	}
	
}
