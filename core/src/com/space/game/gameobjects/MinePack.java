package com.space.game.gameobjects;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Polygon;
import com.space.game.SpriteLoader;

public class MinePack extends Collectible {
	
	public int mines;
	
	public MinePack(float x, float y) {
		this.x = x;
		this.y = y;
		mines = 3;
		sprite = new Sprite(SpriteLoader.minePack);
		float scale = 0.4f;
		width = sprite.getWidth() * scale;
		height = sprite.getHeight() * scale;
		sprite.setScale(scale);
		float points[] =  { 2, 2, 2, height - 2, width - 2, height - 2, width - 2, 2 };
		collider = new Polygon(points);
		setSpritePosition();
		setColliderPosition();
	}
	
	@Override
	public void update(float delta) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void collect(PlayerShip player) {
		// TODO Auto-generated method stub
		player.mines += mines;
		player.hud.update(player.lives, player.mines);
	}

}
