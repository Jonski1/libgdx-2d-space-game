package com.space.game.gameobjects;

public class HealthBar {
	public float x, y;
	public float offset_x, offset_y;
	public float width, height;
	public int amount;
	
	public HealthBar(int amount, float x, float y, float offset_x, float offset_y) {
		this.amount = amount;
		this.offset_x = offset_x;
		this.offset_y = offset_y;
		width = 6;
		height = 6;
		this.x = x + offset_x;
		this.y = y + offset_y;
	}
	
	public void updatePosition(float x, float y) {
		this.x = x + offset_x;
		this.y = y + offset_y;
	}
}
