package com.space.game.gameobjects;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.space.game.GameScreen;
import com.space.game.SpriteLoader;

public class Laser extends Projectile {

	public float speed = 600;
	
	public Laser(float x, float y, float angle) {
		damage = 1;
		explosionSize = 32;
		float cos = MathUtils.cosDeg(angle + 90);
		float sin = MathUtils.sinDeg(angle + 90);
		this.x = x;
		this.y = y;
		velX = speed * cos;
		velY = speed * sin;
		sprite = new Sprite(SpriteLoader.laser);
		float scale = 0.7f;
		width = sprite.getWidth() * scale;
		height = sprite.getHeight() * scale;
		float[] points = { 2, 2, 2, height - 2, width - 2, height - 2, width - 2, 2 };
		collider = new Polygon(points);
		collider.setRotation(angle);
		setColliderPosition();
		collider.setOrigin(width / 2, height / 2);
		sprite.setScale(scale);
		sprite.setOrigin(sprite.getWidth() / 2, sprite.getHeight() / 2);
		sprite.setRotation(angle);
		sprite.setPosition(x - sprite.getWidth() / 2, y - sprite.getHeight() / 2);
	}
	
	@Override
	public void update(float delta) {
		x += velX * delta;
		y += velY * delta;
		sprite.setPosition(x - sprite.getWidth() / 2, y - sprite.getHeight() / 2);
		setColliderPosition();
		//Check bounds
		if(x > GameScreen.width + 100) {
			remove = true;
		}
		if(x < -100) {
			remove = true;
		}
		if(y > GameScreen.height + 100) {
			remove = true;
		}
		if(y < -100) {
			remove = true;
		}
	}

}
