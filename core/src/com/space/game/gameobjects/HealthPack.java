package com.space.game.gameobjects;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Polygon;
import com.space.game.SpriteLoader;

public class HealthPack extends Collectible {
	
	private int heal;
	
	public HealthPack(float x, float y) {
		this.x = x;
		this.y = y;
		heal = 2;
		sprite = new Sprite(SpriteLoader.healthPack);
		float scale = 0.5f;
		width = sprite.getWidth() * scale;
		height = sprite.getHeight() * scale;
		sprite.setScale(scale);
		float[] points =  { 2, 2, 2, height - 2, width - 2, height - 2, width - 2, 2 };
		collider = new Polygon(points);
		setSpritePosition();
		setColliderPosition();
	}
	
	@Override
	public void update(float delta) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void collect(PlayerShip player) {
		// TODO Auto-generated method stub
		player.addHealth(heal);
	}

}
