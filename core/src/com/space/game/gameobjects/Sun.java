package com.space.game.gameobjects;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Circle;
import com.space.game.SpriteLoader;

public class Sun extends SkyOrb {
	
	public Sun(float x, float y) {
		this.x = x;
		this.y = y;
		width = 200f;
		height = 200f;
		mass = 8000;
		collider = new Circle(x, y, width / 2 - 15);
		sprite = new Sprite(SpriteLoader.sun);
		float scale = width / sprite.getWidth();
		sprite.setScale(scale);
		sprite.setPosition(x - sprite.getWidth() / 2, y - sprite.getHeight() / 2);
		sprite.setOrigin(sprite.getWidth() / 2, sprite.getHeight() / 2);
	}
	
	public void update(float delta) {
		
	}
}
