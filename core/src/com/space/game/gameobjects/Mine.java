package com.space.game.gameobjects;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Polygon;
import com.space.game.SpriteLoader;

public class Mine extends Projectile{

	public Mine(float x, float y) {
		damage = 3;
		explosionSize = 64;
		this.x = x;
		this.y = y;
		sprite = new Sprite(SpriteLoader.mine);
		width = sprite.getWidth();
		height = sprite.getHeight();
		//octagon collider
		float[] points = { width / 3, 2, width / 7, height / 3, width / 7, height - height / 3, width / 3, height - 2, 
				width - width / 3, height - 2, width - width / 7, height - height / 3, width - width / 7, height / 3, width - width / 3, 2 };
		collider = new Polygon(points);
		setSpritePosition();
		setColliderPosition();
	}
	
	@Override
	public void update(float delta) {
		
	}

}
