package com.space.game.gameobjects;

import com.badlogic.gdx.graphics.g2d.Sprite;

public abstract class GameObject {
	public Sprite sprite;
	public float width;
	public float height;
	public float x;
	public float y;
	public float velX, velY;
	public float mass;
	
	abstract public void update(float delta);
	
	public void stop() {
		velX = 0;
		velY = 0;
	}
	
	protected void setSpritePosition() {
		sprite.setPosition(x - sprite.getWidth() / 2, y - sprite.getHeight() / 2);
	}
}
