package com.space.game.gameobjects;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.space.game.GameScreen;

public abstract class Alien extends GameObject {
	
	protected PlayerShip target;
	public long laserWait;
	protected int laserCooldownMin;
	protected int laserCooldownMax;
	public boolean remove = false;
	public int health;
	public int maxHealth;
	public Polygon collider;
	public float speed;
	public float explosionSize;
	
	public boolean canFire() {
		if(x < 0 || x > GameScreen.width || y < 0 || y > GameScreen.height) return false;
		return laserWait <= 0;
	}
	
	public Vector2 getDirection() {
		Vector2 vec = new Vector2(target.x - x, target.y - y);
		return vec.nor();
	}
	
	protected void setColliderPosition() {
		collider.setPosition(x - width / 2, y - height / 2);
	}
	
	public void fire() {
		laserWait = (long)MathUtils.random(laserCooldownMin, laserCooldownMax);
	}
	
	public boolean damage(int dmg) {
		health -= dmg;
		if(health < 1) {
			remove = true;
			return true;
		}
		return false;
	}
}
