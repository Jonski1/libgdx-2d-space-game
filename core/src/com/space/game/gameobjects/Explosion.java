package com.space.game.gameobjects;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

public class Explosion {
	public float x, y;
	public float width, height;
	public float stateTime;
	public Animation anim;
	private static final float frameTime = 0.007f;
	
	public Explosion(float x, float y, float width, float height, Array<TextureRegion> frames) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		stateTime = 0;
		anim = new Animation(frameTime, frames);
	}
	
	public void updateStateTime(float delta) {
		stateTime += delta;
	}
}
