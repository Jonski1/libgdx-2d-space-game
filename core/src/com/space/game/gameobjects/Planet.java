package com.space.game.gameobjects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.space.game.SpriteLoader;

public class Planet extends SkyOrb {

	public GameObject orbitTarget;
	public float velocity;
	public float a;
	
	public Planet(float x, float y, GameObject orbitTarget) {
		this.x = x;
		this.y = y;
		mass = 3000;
		this.orbitTarget = orbitTarget;
		float R = Vector2.dst(x, y, orbitTarget.x, orbitTarget.y);
		float b = 0.0005f;
		//T^2 = bR^3
		float T = (float)Math.sqrt(b * R * R * R);
		velocity = MathUtils.PI2 * R / T;
		a = velocity * velocity / R;
		Texture tex = null;
		switch(MathUtils.random(3)) {
		case 0:
			tex = SpriteLoader.planetDesert;
			break;
		case 1:
			tex = SpriteLoader.planetInferno;
			break;
		case 2:
			tex = SpriteLoader.planetMethane;
			break;
		default:
			tex = SpriteLoader.planetTundra;
			break;
		}
		sprite = new Sprite(tex);
		sprite.setPosition(x - sprite.getWidth() / 2, y - sprite.getHeight() / 2);
		sprite.setOrigin(sprite.getWidth() / 2, sprite.getHeight() / 2);
		this.width = sprite.getWidth();
		this.height = sprite.getHeight();
		collider = new Circle(x, y, sprite.getWidth() / 2 - 5);
	}
	
	public void update(float delta) {
		if(orbitTarget != null) {
			float angle = MathUtils.atan2(orbitTarget.y - y, orbitTarget.x - x);
			velX = MathUtils.cos(angle) * a * delta;
			velY = MathUtils.sin(angle) * a * delta;
			angle -= MathUtils.PI / 2;
			velX += MathUtils.cos(angle) * velocity;
			velY += MathUtils.sin(angle) * velocity;
		//	System.out.println(angle);
		}
		x += velX * delta;
		y += velY * delta;
		sprite.setPosition(x - sprite.getWidth() / 2, y - sprite.getHeight() / 2);
		collider.setPosition(x, y);
	}
	
	public String toString() {
		return "Planet";
	}

}
