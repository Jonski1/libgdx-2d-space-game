package com.space.game.gameobjects;

import com.badlogic.gdx.math.Polygon;

public abstract class Projectile extends GameObject {
	public Polygon collider;
	public boolean remove = false;
	public int damage;
	public float explosionSize;
	public boolean alienProjectile = false;
	
	protected void setColliderPosition() {
		collider.setPosition(x - width / 2, y - height / 2);
	}
}
