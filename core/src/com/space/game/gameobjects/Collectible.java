package com.space.game.gameobjects;

import com.badlogic.gdx.math.Polygon;

public abstract class Collectible extends GameObject {
	public Polygon collider;
	public boolean remove = false;
	
	public abstract void collect(PlayerShip player);
	
	protected void setColliderPosition() {
		collider.setPosition(x - width / 2, y - height / 2);
	}
}
