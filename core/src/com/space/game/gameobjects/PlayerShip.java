package com.space.game.gameobjects;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.space.game.GameScreen;
import com.space.game.HUDComponent;
import com.space.game.SpriteLoader;

public class PlayerShip extends GameObject {
	
	public float engineForce = 3.8f;
	public float rotationForce = 200f;
	public static final float G = 5;
	public GameObject closest, secondClosest;
	public int playerNumber;
	public Polygon collider;
	public final float explosionSize = 128;
	//Cooldowns in ms
	public long laserCooldown = 200;
	public long laserWait = 0;
	public long mineCooldown = 500;
	public long mineWait = 0;
	//Health
	private static final int maxHealth = 4;
	public int health;
	public int mines;
	public HealthBar bar;
	//Lives
	public int lives;
	//HUD updates
	public HUDComponent hud;
	private GameScreen game;
	
	public PlayerShip(float x, float y, int playerNumber, int lives, HUDComponent comp, GameScreen game) {
		this.x = x;
		this.y = y;
		this.playerNumber = playerNumber;
		this.lives = lives;
		mines = 2;
		width = 25f;
		height = 22f;
		mass = 50;
		health = maxHealth;
		bar = new HealthBar(maxHealth, x, y, -width / 2 - 3, height / 2 + 10);
		this.game = game;
		this.hud = comp;
		comp.update(lives, mines);
		closest = null;
		float[] points = { 2, 3, width/2, height - 2, width - 2, 3 };
		collider = new Polygon(points);
		collider.setPosition(x - width / 2, y - height / 2);
		collider.setOrigin(width / 2, width / 2);
		sprite = new Sprite(SpriteLoader.ship);
		float scale = width / sprite.getWidth();
		sprite.setScale(scale);
		setSpritePosition();
		sprite.setOrigin(sprite.getWidth() / 2, sprite.getHeight() / 2);
		sprite.setRotation(MathUtils.random(360));
		switch(playerNumber) {
		case 1:
			break;
		case 2:
			sprite.setColor(1f, 0, 0, 1);
			break;
		case 3:
			sprite.setColor(0, 1, 0, 1);
			break;
		default:
			sprite.setColor(0, 0, 1f, 1);
			break;
		}
	}
	
	public void update(float delta) {
		if(closest != null) {
			float dist = Vector2.dst(x, y, closest.x, closest.y);
			float dist2 = Vector2.dst(x, y, secondClosest.x, secondClosest.y);
			float angle = MathUtils.atan2(closest.y -y, closest.x - x);
			float angle2 = MathUtils.atan2(secondClosest.y - y, secondClosest.x - x);
			float vX = G * delta * (closest.mass / (dist * dist) * MathUtils.cos(angle) + secondClosest.mass / (dist2 * dist2) * MathUtils.cos(angle2));
			float vY = G * delta * (closest.mass / (dist * dist) * MathUtils.sin(angle) + secondClosest.mass / (dist2 * dist2) * MathUtils.sin(angle2));
			accelerate(vX, vY);
		}
		x += velX;
		y += velY;
		setSpritePosition();
		collider.setPosition(x - width / 2, y - height / 2);
		collider.setRotation(sprite.getRotation());
		if(laserWait > 0) laserWait -= delta * 1000;
		if(mineWait > 0) mineWait -= delta * 1000;
		//Check bounds
/*		if(x > GameScreen.width + width * 4) {
			restartPlayer();
		}
		if(x <  -width * 4) {
			restartPlayer();
		}
		if(y > GameScreen.height + height * 4) {
			restartPlayer();
		}
		if(y < -height * 4) {
			restartPlayer();
		} */
		if(x > GameScreen.width - width/2) {
			stop();
			x = GameScreen.width - width / 2;
		}
		if(x < width / 2) {
			stop();
			x = width / 2;
		}
		if(y > GameScreen.height - height / 2) {
			stop();
			y = GameScreen.height - height / 2;
		}
		if(y < height / 2) {
			stop();
			y = height / 2;
		}
		bar.updatePosition(x, y);
	}
	
	public void accelerate(float x, float y) {
		velX += x;
		velY += y;
	}
	
	public void rotate(float delta) {
		sprite.rotate(rotationForce * delta);
	}
	
	//Add 90 degrees to rotation since the player sprite is pointed upwards
	public float getRotation() {
		return sprite.getRotation() + 90f;
	}
	
	public void setClosest(Array<SkyOrb> planets) {
		GameObject closestPlanet = null;
		float closestDistance = 0;
		for(GameObject obj : planets) {
			if(closestPlanet == null) {
				closestPlanet = obj;
				closestDistance = Vector2.dst2(x, y, obj.x, obj.y);
				continue;
			}
			float dist = Vector2.dst2(x, y, obj.x, obj.y);
			if(dist < closestDistance) {
				closestPlanet = obj;
				closestDistance = dist;
			}
		}
		closest = closestPlanet;
		GameObject secondClosestPlanet = null;
		float secondClosestDistance = 0;
		for(GameObject obj : planets) {
			if(obj == closestPlanet) continue;
			if(secondClosestPlanet == null) {
				secondClosestPlanet = obj;
				secondClosestDistance = Vector2.dst2(x, y, obj.x, obj.y);
				continue;
			}
			float dist = Vector2.dst2(x, y, obj.x, obj.y);
			if(dist < secondClosestDistance) {
				secondClosestPlanet = obj;
				secondClosestDistance = dist;
			}
		}
		secondClosest = secondClosestPlanet;
	}
	
	public boolean canFire() {
		return laserWait <= 0;
	}
	
	public void fire() {
		laserWait = laserCooldown;
	}
	
	public void dropMine() {
		mineWait = mineCooldown;
		mines--;
		hud.update(lives, mines);
	}
	
	public boolean canDropMine() {
		return mineWait <= 0 && mines > 0;
	}
	
	public void planetCollision() {
		restartPlayer();
	}
	//return true if dead
	public boolean projectileCollision(int damage) {
		health -= damage;
		if(health <= 0) {
			restartPlayer();
			return true;
		}
		bar.amount = health;
		return false;
	}
	
	public void restartPlayer() {
		//Negative score for dying
		if(game.singlePlayer) game.spHandler.updateScore(-30);
		lives--;
		stop();
		Vector2 pos = game.getSafePosition();
		x = pos.x;
		y = pos.y;
		sprite.setRotation(MathUtils.random(360));
		mines = 2;
		hud.update(lives, mines);
		health = maxHealth;
		bar.amount = health;
	}
	
	public void addHealth(int heal) {
		if((health + heal) > maxHealth) health = maxHealth;
		else health += heal;
		bar.amount = health;
	}
}
