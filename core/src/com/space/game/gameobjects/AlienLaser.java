package com.space.game.gameobjects;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.space.game.GameScreen;
import com.space.game.SpriteLoader;

public class AlienLaser extends Projectile {
	
	public float speed;
	
	public AlienLaser(float x, float y, Vector2 target) {
		this.x = x;
		this.y = y;
		speed = 450;
		damage = 1;
		explosionSize = 16f;
		alienProjectile = true;
		velX = speed * target.x;
		velY = speed * target.y;
		sprite = new Sprite(SpriteLoader.alienProjectile);
		width = sprite.getWidth();
		height = sprite.getHeight();
		float[] points = { width / 3, 2, width / 7, height / 3, width / 7, height - height / 3, width / 3, height - 2, 
				width - width / 3, height - 2, width - width / 7, height - height / 3, width - width / 7, height / 3, width - width / 3, 2 };
		collider = new Polygon(points);
		setColliderPosition();
		setSpritePosition();
		//collider.setOrigin(width / 2, height / 2);
	}
	
	@Override
	public void update(float delta) {
		// TODO Auto-generated method stub
		x += velX * delta;
		y += velY * delta;
		setSpritePosition();
		setColliderPosition();
		//Check bounds
		if(x > GameScreen.width + 100) {
			remove = true;
		}
		if(x < -100) {
			remove = true;
		}
		if(y > GameScreen.height + 100) {
			remove = true;
		}
		if(y < -100) {
			remove = true;
		}
	}

}
