package com.space.game.gameobjects;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.space.game.SpriteLoader;

public class AlienRoundShip extends Alien {
	
	public AlienRoundShip(float x, float y, PlayerShip target, int health) {
		this.x = x;
		this.y = y;
		this.target = target;
		laserWait = 0;
		laserCooldownMax = 1400;
		laserCooldownMin = 950;
		speed = 180f;
		maxHealth = health;
		this.health = maxHealth;
		sprite = new Sprite(SpriteLoader.roundAlien);
		width = sprite.getWidth();
		height = sprite.getHeight();
		explosionSize = width;
		float[] points = { width / 3, 2, width / 7, height / 3, width / 7, height - height / 3, width / 3, height - 2, 
				width - width / 3, height - 2, width - width / 7, height - height / 3, width - width / 7, height / 3, width - width / 3, 2 };
		collider = new Polygon(points);
		collider.setPosition(x - width / 2, y - height / 2);
		collider.setOrigin(width / 2, width / 2);
		setSpritePosition();
	}
	
	@Override
	public void update(float delta) {
		// TODO Auto-generated method stub
		Vector2 vec = getDirection();
		Vector2 vel = new Vector2(velX, velY);
		vel.add(vec.scl(speed));
		vel.limit(speed);
		velX = vel.x;
		velY = vel.y;
		x += velX * delta;
		y += velY * delta;
		setSpritePosition();
		setColliderPosition();
		if(laserWait > 0) laserWait -= delta * 1000f;
	}

}
