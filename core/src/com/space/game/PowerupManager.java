package com.space.game;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.space.game.gameobjects.Collectible;
import com.space.game.gameobjects.HealthPack;
import com.space.game.gameobjects.MinePack;

public class PowerupManager {
	
	public GameScreen game;
	//Time is in seconds
	private float timeWaited;
	private float timeToWait;
	private int maxCollectibles;
	
	public PowerupManager(GameScreen game, int players) {
		this.game = game;
		timeToWait = MathUtils.random(8, 20);
		timeWaited = 0;
		maxCollectibles = 3 * players;
	}
	
	//Checks if a new collectible "powerup" should be spawned
	public void update(float delta) {
		timeWaited += delta;
		if(timeWaited >= timeToWait) {
			if(game.getCollectiblesAmount() < maxCollectibles) {
				Vector2 pos = game.getSafePosition();
				Collectible c;
				switch(MathUtils.random(2)) {
				case 0:
					c = new MinePack(pos.x, pos.y);
					break;
				default:
					c = new HealthPack(pos.x, pos.y);
					break;
				}
				game.createCollectible(c);
			}
			timeWaited = 0;
			timeToWait = MathUtils.random(8, 20);
		}
	}
}
