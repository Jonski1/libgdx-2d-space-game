package com.space.game;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class HUDComponent extends Actor {
	
	private Texture backGround, ship, mine;
	private float width, height;
	private float shipX, shipY;
	private float mineX, mineY;
	private String lifeString, mineString;
	public int plrNum;
	
	public HUDComponent(float x, float y, int plrNum) {
		super();
		this.plrNum = plrNum;
		Pixmap p = new Pixmap(1, 1, Format.RGBA8888);
		switch(plrNum) {
		case 1:
			p.setColor(0.8f, 0.7f, 0.6f, 0.4f);
			break;
		case 2:
			p.setColor(1, 0, 0, 0.4f);
			break;
		case 3:
			p.setColor(0, 1, 0, 0.4f);
			break;
		case 4:
			p.setColor(0, 0, 1, 0.4f);
			break;
		}
		p.fill();
		backGround = new Texture(p);
		p.dispose();
		ship = SpriteLoader.ship;
		mine = SpriteLoader.mine;
		setPosition(x, y);
		width = 170;
		height = 50;
		shipX = 5;
		shipY = height / 3;
		mineX = width / 2;
		mineY = height / 3;	
	}
	
	public void update(int lives, int mines) {
		lifeString = "x " + lives;
		mineString = "x " + mines;
		if(lives < 0) {
			lifeString = "DEAD";
			mineString = "";
		}
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		batch.draw(backGround, getX(), getY(), width, height);
		batch.draw(ship, getX() + shipX, getY() + shipY);
		batch.draw(mine, getX() + mineX, getY() + mineY);
		SpriteLoader.HUDFont.draw(batch, lifeString, getX() + shipX + 30, getY() + shipY + 15);
		SpriteLoader.HUDFont.draw(batch, mineString, getX() + mineX + 23, getY() + mineY + 15);
	}
	
}
