package com.space.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;

public class EndScreen implements Screen {

	public SpaceGame game;
	public String message;
	public OrthographicCamera cam;
	
	public EndScreen(SpaceGame game, String message) {
		this.game = game;
		this.message = message;
		cam = new OrthographicCamera(game.res_x, game.res_y);
		cam.position.set(game.res_x / 2, game.res_y / 2, 0);
		float scale = game.res_x / 1024f * 2f;
		SpriteLoader.HUDFont.setScale(scale);
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		cam.update();
		game.batch.setProjectionMatrix(cam.combined);
		game.batch.begin();
		float x = game.res_x / 3;
		float y = game.res_y / 2 + 20;
		SpriteLoader.HUDFont.drawMultiLine(game.batch, message, x, y);
		game.batch.end();
		if(Gdx.input.isKeyPressed(Keys.ESCAPE)) Gdx.app.exit();
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void show() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		SpriteLoader.HUDFont.dispose();
	}
}
