package com.space.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class SpaceGame extends Game {
	public SpriteBatch batch;
	
	public int width, height;
	public int players;
	public int res_x, res_y;
	public int size;
	public int lives;
	public int planets;
	
	public SpaceGame(int players, int size, int res_x, int res_y, int lives) {
		super();
		this.players = players;
		this.size = size;
		this.res_x = res_x;
		this.res_y = res_y;
		this.lives = lives;
		switch(size) {
		case 1:
			planets = 1;
			width = 1200;
			height = 1000;
			break;
		case 2:
			planets = 3;
			width = 1600;
			height = 1400;
			break;
		default:
			planets = 4;
			width = 2000;
			height = 1700;
			break;
		}
	}
	
	@Override
	public void create () {
		batch = new SpriteBatch();
		Gdx.input.setCursorCatched(true);
		this.setScreen(new GameScreen(this, width, height, players, res_x, res_y, lives, planets));
	}

	@Override
	public void render () {
		super.render();
	}
	
	public void dispose() {
		batch.dispose();
	}
}
