package com.space.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.space.game.gameobjects.Alien;
import com.space.game.gameobjects.AlienRoundShip;
import com.space.game.gameobjects.Collectible;
import com.space.game.gameobjects.Explosion;
import com.space.game.gameobjects.GameObject;
import com.space.game.gameobjects.AlienLaser;
import com.space.game.gameobjects.Laser;
import com.space.game.gameobjects.Mine;
import com.space.game.gameobjects.Planet;
import com.space.game.gameobjects.PlayerShip;
import com.space.game.gameobjects.Projectile;
import com.space.game.gameobjects.SkyOrb;
import com.space.game.gameobjects.Sun;

public class GameScreen implements Screen {

	public SpaceGame game;
	public static int width, height;
	//Containers
	private Array<GameObject> gameObjects;
	private Array<SkyOrb> planets;
	private Array<Projectile> projectiles;
	private Array<Explosion> explosions;
	private Array<Collectible> collectibles;
	private Array<Alien> aliens;
	private OrthographicCamera cam;
	public static final boolean debug = false;
	//Android stuff
	public static boolean android = false;
	public AndroidInput androin;
	
	//Players
	private Array<PlayerShip> players;
	public final int startingPlayers;
	public int score;
	//Collider debugging
	private ShapeRenderer sr;
	//Stage-actor-test
	private Stage stage;
	//Collectible handler
	private PowerupManager powerups;
	//Single player
	public boolean singlePlayer;
	public SinglePlayerHandler spHandler;
	
	public GameScreen(SpaceGame game, int width, int height, int plrs, int res_x, int res_y, int lives, int numberPlanets) {
		this.game = game;
		GameScreen.width = width;
		GameScreen.height = height;
		cam = new OrthographicCamera(res_x, res_y);
		//Stage
		stage = new Stage(new ScreenViewport());
		if(android) {
			Gdx.input.setInputProcessor(stage);
			androin = new AndroidInput(stage.getWidth(), stage.getHeight());
			Actor[] actors = androin.get();
			for(Actor a : actors) {
				stage.addActor(a);
			}
		}
		//Initialize sprites
		SpriteLoader.loadSprites();
		//Arrays
		gameObjects = new Array<GameObject>();
		players = new Array<PlayerShip>();
		//Check for single player mode
		startingPlayers = plrs;
		if(plrs == 1) {
			singlePlayer = true;
			spHandler = new SinglePlayerHandler(this);
			score = 0;
			spHandler.label.setX(stage.getWidth() - 100);
			spHandler.label.setY(stage.getHeight() - 30);
			stage.addActor(spHandler.label);
		}
		else singlePlayer = false;
		planets = new Array<SkyOrb>();
		projectiles = new Array<Projectile>();
		explosions = new Array<Explosion>();
		collectibles = new Array<Collectible>();
		aliens = new Array<Alien>();
		powerups = new PowerupManager(this, plrs);
		//Game objects
		Sun sun = createSun(width / 2, height / 2);
		for(int i = 0; i < numberPlanets; i++) {
			createPlanet(300f * (i + 1), sun);
		}
		for(int i = 0; i < plrs; i++) {
			Vector2 pos = getSafePosition();
			addPlayer(pos.x, pos.y, i + 1, lives);
		}
		//Collider renderer
		if(debug) sr = new ShapeRenderer();
	}
	
	@Override
	public void render(float delta) {
		//Clear the screen
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		//Draw sprites
		game.batch.setProjectionMatrix(cam.combined);
		game.batch.begin();
		game.batch.draw(SpriteLoader.backGround, 0, 0, width, height);
		for(GameObject obj : gameObjects) {
			obj.sprite.draw(game.batch);
		}
		for(Explosion exp : explosions) {
			game.batch.draw(exp.anim.getKeyFrame(exp.stateTime), exp.x, exp.y, exp.width, exp.height);
		}
		for(PlayerShip player : players) {
			float x = player.bar.x;
			float y = player.bar.y;
			float width = player.bar.width;
			float height = player.bar.height;
			for(int i = 0; i < player.bar.amount; i++)
				game.batch.draw(SpriteLoader.healthBar, x + (width + 2) * i, y, width, height);
		}
		game.batch.end();
		
		//Draw colliders
		if(debug) {
			sr.setProjectionMatrix(cam.combined);
			sr.begin(ShapeType.Line);
			sr.setColor(0, 1, 0, 1);
			for(PlayerShip p : players) {
				sr.polygon(p.collider.getTransformedVertices());
			}
			for(SkyOrb orb : planets) {
				sr.circle(orb.collider.x, orb.collider.y, orb.collider.radius);
			}
			for(Projectile proj : projectiles) {
				sr.polygon(proj.collider.getTransformedVertices());
			}
			for(Collectible col : collectibles) {
				sr.polygon(col.collider.getTransformedVertices());
			}
			for(Alien a : aliens) {
				sr.polygon(a.collider.getTransformedVertices());
			}
			sr.end();
		}
		//Stage for the HUD
		stage.draw();
		//Camera
		updateCamera(delta);
		//Update movement etc
		update(delta);
		//Update victory conditions
		victoryConditions();
	}
	
	private void update(float delta) {
		//Input
		playerInput(delta);
		if(Gdx.input.isKeyPressed(Keys.ESCAPE))
			Gdx.app.exit();
			//Zoom
		if(debug) {
			if(Gdx.input.isKeyPressed(Keys.PERIOD)) {
				cam.zoom += 0.02f;
				if(cam.viewportWidth * cam.zoom > width) 
					cam.zoom = width / cam.viewportWidth;
				if(cam.viewportHeight * cam.zoom > height)
					cam.zoom = height / cam.viewportHeight;
			}
			if(Gdx.input.isKeyPressed(Keys.MINUS)) {
				cam.zoom -= 0.02f;
				if(cam.zoom < 0.2f) cam.zoom = 0.2f;
			}
		}
		//Powerup manager update
		powerups.update(delta);
		//Single player handler update
		if(singlePlayer) spHandler.update(delta);
		//Movement update
		for(GameObject obj : gameObjects) {
			obj.update(delta);
		}
		//Alien firing
		alienFiring();
		//Collision
		playerCollision();
		alienCollision();
		//Remove projectiles
		for(Projectile proj : projectiles) {
			if(proj.remove)
				removeProjectile(proj);
		}
		//Remove collectibles
		for(Collectible col : collectibles) {
			if(col.remove)
				removeCollectible(col);
		}
		//Remove aliens
		for(Alien a : aliens) {
			if(a.remove)
				removeAlien(a);
		}
		//Update explosions
		for(Explosion exp : explosions) {
			if(exp.anim.isAnimationFinished(exp.stateTime)) {
				explosions.removeValue(exp, true);
				continue;
			}
			exp.updateStateTime(delta);
		}
	}

	private void alienFiring() {
		for(Alien a : aliens) {
			if(a.canFire()) {
				a.fire();
				float offset = 10;
				Vector2 dir = a.getDirection();
				float offset_x = dir.x * offset;
				float offset_y = dir.y * offset;
				AlienLaser laser = new AlienLaser(a.x + offset_x, a.y + offset_y, dir);
				gameObjects.add(laser);
				projectiles.add(laser);
			}
		}
	}
	
	private void playerInput(float delta) {
		//Player input
		for(PlayerShip player : players) {
			float rotation = player.getRotation();
			int num = player.playerNumber;
			if(Gdx.input.isKeyPressed(Controls.Up(num)) || (android && androin.isPressed("up") && num == 1)) {
				float accY = player.engineForce * delta * MathUtils.sinDeg(rotation);
				float accX = player.engineForce * delta * MathUtils.cosDeg(rotation);
				player.accelerate(accX, accY);
			}
			if(Gdx.input.isKeyPressed(Controls.Down(num)) || (android && androin.isPressed("down") && num == 1)) {
				float accY = -player.engineForce * delta * MathUtils.sinDeg(rotation);
				float accX = -player.engineForce * delta * MathUtils.cosDeg(rotation);
				player.accelerate(accX, accY);
			}
			if(Gdx.input.isKeyPressed(Controls.Left(num)) || (android && androin.isPressed("left") && num == 1)) {
				player.rotate(delta);
			}
			if(Gdx.input.isKeyPressed(Controls.Right(num)) || (android && androin.isPressed("right") && num == 1)) {
				player.rotate(-delta);
			}
			//Fire!
			if(Gdx.input.isKeyPressed(Controls.Fire(num)) || (android && androin.isPressed("fire") && num == 1)) {
				if(player.canFire()) {
					player.fire();
					//Shoot the beam from the collider's top vertex
					float vertices[] = player.collider.getTransformedVertices();
					Laser laser = new Laser(vertices[2], vertices[3], player.sprite.getRotation());
					projectiles.add(laser);
					gameObjects.add(laser);
				}
			}
			//Bombs away!
			if(Gdx.input.isKeyPressed(Controls.Bomb(num)) || (android && androin.isPressed("bomb") && num == 1)) {
				if(player.canDropMine()) {
					player.dropMine();
					//Drop the mine from the tail
					float vertices[] = player.collider.getTransformedVertices();
					float x = (vertices[0] + vertices[4]) / 2 - MathUtils.cosDeg(player.getRotation()) * 12;
					float y = (vertices[1] + vertices[5]) / 2 - MathUtils.sinDeg(player.getRotation()) * 12;
					Mine mine = new Mine(x, y);
					projectiles.add(mine);
					gameObjects.add(mine);
				}
			}
			player.setClosest(planets);
		}
	}
	
	private void playerCollision() {
		//Collision
		for(PlayerShip player : players) {
			float[] vertices = player.collider.getTransformedVertices();
			//Planets
			for(SkyOrb orb : planets) {
				for(int i = 0; i < vertices.length; i++) {
					if(orb.collider.contains(vertices[i], vertices[++i])) {
						player.planetCollision();
						createExplosion(vertices[i - 1], vertices[i], player.explosionSize, player.explosionSize);
						break;
					}
				}
			}
			//Projectiles
			for(Projectile proj : projectiles) {
				float[] pVertices = proj.collider.getTransformedVertices();
				for(int i = 0; i < pVertices.length; i++) {
					if(player.collider.contains(pVertices[i], pVertices[++i])) {
						createExplosion(pVertices[i - 1], pVertices[i], proj.explosionSize, proj.explosionSize);
						float last_x = player.x;
						float last_y = player.y;
						if(player.projectileCollision(proj.damage))
							createExplosion(last_x, last_y, player.explosionSize, player.explosionSize);
						proj.remove = true;
						break;
					}
				}
			} 
			//Collectibles
			for(Collectible col : collectibles) {
				float[] cVertices = col.collider.getTransformedVertices();
				for(int i = 0; i < cVertices.length; i++) {
					if(player.collider.contains(cVertices[i], cVertices[++i])) {
						col.collect(player);
						col.remove = true;
						break;
					}
				}
			}
			//Aliens
			for(Alien a : aliens) {
				for(int i = 0; i < vertices.length; i++) {
					if(a.collider.contains(vertices[i], vertices[++i])) {
						float last_x = player.x;
						float last_y = player.y;
						if(player.projectileCollision(2))
							createExplosion(last_x, last_y, player.explosionSize, player.explosionSize);
						a.damage(100);
						//10 score for killing an alien by crashing on it
						spHandler.updateScore(10);
						createExplosion(a.x, a.y, a.explosionSize, a.explosionSize);
						break;
					}
				}
			}
		}
	}
	
	private void alienCollision() {
		for(Alien a : aliens) {
			for(Projectile proj : projectiles) {
				if(proj.alienProjectile) continue;
				float[] pVertices = proj.collider.getTransformedVertices();
				for(int i = 0; i < pVertices.length; i++) {
					if(a.collider.contains(pVertices[i], pVertices[++i])) {
						createExplosion(pVertices[i - 1], pVertices[i], proj.explosionSize, proj.explosionSize);
						if(a.damage(proj.damage)) {
							//Health * 10 score for killing an alien
							spHandler.updateScore(a.maxHealth * 10);
							createExplosion(a.x, a.y, a.explosionSize, a.explosionSize);
						}
						proj.remove = true;
						break;
					}
				}
			}
		}
	}
	
	private void updateCamera(float delta) {
		float avg_x = 0;
		float avg_y = 0;
		float border = 150;
		float furthest_x = -1;
		float furthest_y = -1;
		for(PlayerShip player : players) {
			avg_x += player.x;
			avg_y += player.y;
		}
		avg_x /= players.size;
		avg_y /= players.size;
		for(PlayerShip player : players) {
			if(furthest_x < 0) {
				furthest_x = Math.abs(player.x - avg_x + border);
			}
			else {
				if(Math.abs(player.x - avg_x + border) > furthest_x) furthest_x = Math.abs(player.x - avg_x + border);
			}
			if(furthest_y < 0) {
				furthest_y = Math.abs(player.y - avg_y + border);
			}
			else {
				if(Math.abs(player.y - avg_y + border) > furthest_y) furthest_y = Math.abs(player.y - avg_y + border);
			}
		}
		cam.position.set(MathUtils.lerp(cam.position.x, avg_x, 10 * delta), MathUtils.lerp(cam.position.y, avg_y, 8 * delta), 0);
		if(!debug) {
			float zoomX = furthest_x * 2 / cam.viewportWidth;
			float zoomY = furthest_y * 2 / cam.viewportHeight;
			if(zoomX >= zoomY) cam.zoom = MathUtils.lerp(cam.zoom, zoomX, 4f * delta);
			else cam.zoom = MathUtils.lerp(cam.zoom, zoomY, 4f * delta);
			if(cam.zoom < 1) cam.zoom = MathUtils.lerp(cam.zoom, 1, 4f * delta);
			if(cam.position.x - cam.viewportWidth * cam.zoom / 2 < 0) cam.position.x = cam.viewportWidth * cam.zoom / 2;
			if(cam.position.x + cam.viewportWidth * cam.zoom / 2 > width) cam.position.x = width - cam.viewportWidth * cam.zoom / 2;
			if(cam.position.y - cam.viewportHeight * cam.zoom / 2 < 0) cam.position.y = cam.viewportHeight * cam.zoom / 2;
			if(cam.position.y + cam.viewportHeight * cam.zoom / 2 > height) cam.position.y = height - cam.viewportHeight * cam.zoom / 2;
		}
		cam.update();
	}
	
	public Sun createSun(float x, float y) {
		Sun sun = new Sun(x, y);
		gameObjects.add(sun);
		planets.add(sun);
		return sun;
	}
	
	public void victoryConditions() {
		if(singlePlayer && players.size == 0) endScreen("Your score: " + String.valueOf(score));
		else if(players.size == 0) endScreen("NOBODY LOST!");
		for(PlayerShip plr : players) {
			if(plr.lives < 0) removePlayer(plr);
		}
		if(players.size == 1 && startingPlayers > 1) endScreen("PLAYER " + players.get(0).playerNumber + 
				" WON, CONGRATULATIONS!");
	}
	
	public void endScreen(String message) {
		game.setScreen(new EndScreen(game, new String(message + "\nPRESS ESC TO EXIT")));
		dispose();
	}

	public Vector2 getSafePosition() {
		Vector2 position = new Vector2();
		float x = 0; 
		float y = 0;
		boolean safe = false;
		while(!safe) {
			x = MathUtils.random(40, width - 40);
			y = MathUtils.random(25, height - 25);
			for(int i = 0; i < planets.size; i++) {
				GameObject obj = planets.get(0);
				if(Math.abs((x + y) - (obj.x + obj.y)) < 700) {
					safe = false;
					break;
				}
				safe = true;
			}
		}
		position.set(x, y);
		return position;
	}
	
	public int getCollectiblesAmount() {
		return collectibles.size;
	}
	
	public void addPlayer(float x, float y, int num, int lives) {
		float hudX = 25;
		float hudY;
		if(android) hudY = stage.getHeight() - 55 * num; 
		else hudY = cam.viewportHeight - 55 * num;
		HUDComponent comp = new HUDComponent(hudX, hudY , num);
		PlayerShip ship = new PlayerShip(x, y, num, lives, comp, this);
		gameObjects.add(ship);
		players.add(ship);
		stage.addActor(comp);
	}
	
	public void addAlien(int health) {
		if(players.size == 0) return;
		float x, y;
		switch(MathUtils.random(3)) {
			case 0:
				x = MathUtils.random(width);
				y = -MathUtils.random(80f, 120f);
				break;
			case 1:
				x = MathUtils.random(width);
				y = -MathUtils.random(height + 80f, height + 120f);
				break;
			case 2:
				x = -MathUtils.random(80f, 120f);
				y = MathUtils.random(height);
				break;
			default:
				x = -MathUtils.random(width + 80f, width + 120f);
				y = MathUtils.random(height);
				break;
		}
		AlienRoundShip ars = new AlienRoundShip(x, y, players.get(0), health);
		gameObjects.add(ars);
		aliens.add(ars);
	}
	
	public void removePlayer(PlayerShip p) {
		gameObjects.removeValue(p, true);
		players.removeValue(p, true);
	}
	
	//A planet needs an object to revolve around
	public void createPlanet(float dist, GameObject sun) {
		int angle = MathUtils.random(360);
		float x = MathUtils.cosDeg(angle) * dist + sun.x;
		float y = MathUtils.sinDeg(angle) * dist + sun.y;
		Planet planet = new Planet(x, y, sun);
		gameObjects.add(planet);
		planets.add(planet);
	}
	
	public void createCollectible(Collectible c) {
		gameObjects.add(c);
		collectibles.add(c);
	}
	
	public void createExplosion(float x, float y, float width, float height) {
		explosions.add(new Explosion(x, y, width, height, SpriteLoader.expRegion));
	}
	
	public void removeProjectile(Projectile p) {
		gameObjects.removeValue(p, true);
		projectiles.removeValue(p, true);
	}
	
	public void removeCollectible(Collectible c) {
		gameObjects.removeValue(c, true);
		collectibles.removeValue(c,  true);
	}
	
	public void removeAlien(Alien a) {
		gameObjects.removeValue(a, true);
		aliens.removeValue(a, true);
	}
	
	public boolean noAliens() {
		if(aliens == null) return false;
		return aliens.size == 0;
	}
	
	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void show() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		SpriteLoader.dispose();
		stage.dispose();
	}

}
