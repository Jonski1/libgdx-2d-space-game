# LibGDX 2D space game#

A 2D space shooter with attraction between ships and planets. Made as a university exercise project. Contains two different source directories: A desktop launcher and the game itself. One directory contains stuff for gradle, which is a dependency manager. May contain unused assets. LibGDX (http://libgdx.badlogicgames.com/) needed for compiling.

Link to short youtube clip: https://www.youtube.com/watch?v=1NO6atmRGQI