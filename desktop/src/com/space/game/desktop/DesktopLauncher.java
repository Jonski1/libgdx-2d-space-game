package com.space.game.desktop;

import java.util.Scanner;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.space.game.SpaceGame;
/*
 * Set the game up through the console before starting
 */
public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		String parse;
		int players, size, resolution, lives;
		Scanner scan = new Scanner(System.in);
		//Players
		System.out.println("Players? (1-4)");
		parse = scan.nextLine();
		try {
			players = Integer.valueOf(parse);
		}
		catch (Exception e) {
			players = 2;
		}
		if(players < 1 || players > 4) players = 2;
		//Lives
		System.out.println("Lives?");
		parse = scan.nextLine();
		try {
			lives = Integer.valueOf(parse);
		}
		catch (Exception e) {
			lives = 5;
		}
		if(lives < 0) lives = 0;
		//Map size
		System.out.println("Small, mid or large size? (1-3)");
		parse = scan.nextLine();
		try {
			size = Integer.valueOf(parse);
		}
		catch (Exception e) {
			size = 2;
		}
		if(size < 1 || size > 3) size = 2;
		//Fullscreen
		System.out.println("Fullscreen? (y/n)");
		parse = scan.nextLine();
		if(parse.equals("y")) config.fullscreen = true;
		//Resolution
		System.out.println("Resolution?");
		System.out.println("800x600 [1]");
		System.out.println("1024x768 [2]");
		System.out.println("1280x720 [3]");
		System.out.println("1600x1200[4]");
		System.out.println("1920x1080[5]");
		parse = scan.nextLine();
		try {
			resolution = Integer.valueOf(parse);
		}
		catch (Exception e) {
			resolution = 1;
		}
		int res_x, res_y;
		switch(resolution) {
		case 2:
			res_x = 1024;
			res_y = 768;
			break;
		case 3:
			res_x = 1280;
			res_y = 720;
			break;
		case 4:
			res_x = 1600;
			res_y = 1200;
			break;
		case 5:
			res_x = 1920;
			res_y = 1080;
			break;
		default:
			res_x = 800;
			res_y = 600;
			break;
		}
		scan.close();
		config.width = res_x;
		config.height = res_y;
		new LwjglApplication(new SpaceGame(players, size, res_x, res_y, lives), config);
	}
}
